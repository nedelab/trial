Pas besoin d'écrire de code Python dans ce mini-cas, du pseudo-code clair suffit.

Vous voulez écrire un interpréteur d'expressions glob (merci de ne pas utiliser le module "glob" déjà existant de python).
Une expression glob est une chaine de caractères utilisée pour retrouver les fichiers d'un filesystem qui répondent à certains critères. Par exemple, pour l'expression glob "/home/me/\*2022\*/\*.log", l'interpréteur doit renvoyer tous les fichiers du filesystem dont le nom finit pas ".log" et qui sont situés à la racine d'un dossier qui contient la chaîne de caractères "2022" et qui lui-même se situe dans /home/me/. Vous l'aurez peut-être deviné, le symbole "\*" signifie "n'importe quel caractère sauf '/'". "\*" est le seul caractère spécial du langage glob que l'on utilisera dans ce mini-cas (oublions "?" et "[]").

Pour vous aider, vous avez à votre disposition la fonction fnmatch(path, pattern) qui renvoie True si le chemin d'accès "path" répond au critère décrit par "pattern" (par exemple, fnmatch("2022-12-03", "\*2022\*") == True parce que "2022-12-03" contient "2022", mais fnmatch("2022-12-03", "\*2022") == False car "2022-12-03" ne finit pas par "2022").

Donnez l'algorithme qui prend en entrée une expression glob, et renvoie la liste de fichiers du filesystem qui correspondent à l'expression glob.
