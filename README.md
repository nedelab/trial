## Avertissement préliminaire
MERCI DE NE PAS PUBLIER VOS RÉPONSES.

## Introduction
Ce répo contient des mini-cas pour tester vos compétences.

Un bon développeur doit maîtriser 4 domaines clés :
* l'algorithmie
* la sécurité
* le software design
* les outils (les langages, les IDE, les protocoles réseaux, Git, Docker, etc)

Le dernier point est le plus facile et rapide à apprendre (si la documentation est bonne). Il n'est donc pas testé ici. 

Les 3 autres points ne peuvent s'acquérir qu'à travers une pratique longue et consciencieuse de la programmation. Ce sont donc de loin les compétences les plus valorisables pour un développeur.

Il y a 5 mini-cas :
* algorithmie/glob
* secu/archivage
* secu/distant_shell
* software_design/modelisation_sql
* software_design/racing_game

Certains cas sont très simples, d'autres beaucoup moins.

Il faut compter entre 30min et 2h pour terminer les 5 mini-cas.

Vous avez le droit à Internet.