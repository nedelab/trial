from fastapi import FastAPI, HTTPException, status, UploadFile
from fastapi.responses import FileResponse
import os

app = FastAPI()

@app.get("/file")
def download(path: str):
    if not (os.path.exists(path) and os.path.isfile(path)):
        raise HTTPException(status.HTTP_404_NOT_FOUND, "Le fichier n'existe pas")
    return FileResponse(path)

@app.put("/file")
def upload(path: str, tmp_file: UploadFile):
    with open(path, "wb") as f:
        f.write(tmp_file.file.read())

if __name__ == "__main__":
    import uvicorn
    uvicorn.run("app:app", port=8000, log_level="info")