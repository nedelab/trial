Le code de l'API est dans le fichier app.py.

L'API a été développée en interne par votre entreprise et tourne sur l'un de ses serveurs. Elle permet à un utilisateur distant de déposer des fichiers sur le serveur sur lequel tourne l'API, et de les télécharger plus tard. 

L'API contient 2 routes :
* une route qui renvoie le contenu d'un fichier (GET /file)
* une route qui dépose un fichier (PUT /file)

L'URL de l'API a été donnée au comptable de l'entreprise pour lui permettre d'archiver les factures de l'entreprise et de les retrouver plus tard. Pour simplifier, supposons que le comptable est le seul à avoir accès à l'API (il n'y a donc pas de système d'authentification, et nous n'attendons pas dans ce mini-cas que soit développé un tel système). 

Mais nous avons un problème : le comptable est aussi un bon informaticien et n'est pas toujours digne de confiance.

Analyser le code de l'API et décrivez en quelques phrases une ou deux failles de sécurité béantes.
En particulier, dites comment le comptable pourraient les exploiter pour sérieusement embêter votre entreprise.
Ensuite, réécrivez le code en corrigeant les failles de sécurité. 

