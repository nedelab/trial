Le code de l'API est dans le fichier app.py.

L'API a été développée en interne par votre entreprise et tourne sur l'un de ses serveurs. Elle permet de lancer à distance n'importe quelle commande shell sur le serveur sur lequel tourne l'API.

L'API contient une seule route :
* une route qui permet d'exécuter une commande donnée en paramètre (POST /exec)

Pour éviter que n'importe qui puisse exécuter du shell sur le serveur de l'entreprise, un système d'authentification par token a été implémenté. Vous êtes le seul à connaître ce token, et ne le partagez avec personne.

Analysez le code de l'API, et identifiez une mauvaise pratique qui pourrait facilement compromettre toute la sécurité du système par token. Ensuite, réécrivez le code en corrigeant cette mauvaise pratique. 

