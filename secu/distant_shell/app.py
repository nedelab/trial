from fastapi import FastAPI, HTTPException, status
import os
import json

def is_token_correct(token: str):
    with open("creds.json", "r") as f:
        return token == json.load(f)["token"]

app = FastAPI()

@app.post("/exec")
def execute_shell(cmd: str, token: str):
    if not is_token_correct(token):
        raise HTTPException(status.HTTP_401_UNAUTHORIZED, "Vous n'avez pas les droits")
    os.system(cmd)

if __name__ == "__main__":
    import uvicorn
    uvicorn.run("app:app", port=8000, log_level="info")