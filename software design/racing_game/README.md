Pas besoin d'écrire du code Python dans ce mini-cas. Du pseudo-code, ou une explication claire et concise suffit.

Vous développez un jeu de course. Dans ce jeu, vous voulez donner la possibilité au joueur de customiser sa voiture. En particulier, vous souhaitez qu'il puisse choisir : 
* la couleur de la carosserie ;
* le type de peinture de la carosserie (métallique, mate, etc) ;
* la forme des jantes ;
* la couleur des jantes ;
* le type de peinture des jantes (métallique, mate, etc) ;
* la forme de l'aileron arrière ;
* le type de l'aileron (rétractable, fixe, etc)
* la couleur de l'aileron ;
* et très certainement d'autres choses qui vous viendront en tête au fur et à mesure du projet ;

Vous avez une classe qui se nomme "Car" et qui possède une méthode "draw" qui dessine la voiture à l'écran. 

Comment structureriez-vous votre code, dans les grandes lignes, pour implémenter la méthode "draw" de "Car" ?