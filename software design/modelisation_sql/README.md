Vous souhaitez développer un site web de lecture en ligne (comme www.lirtuose.fr par exemple).

Vous êtes en charge des bases de données. Les développeurs du back et du front ne savent pas encore exactement ce qu'ils vont faire.

Pour simplifier, considérons que vous avez à votre disposition des données concernant les auteurs (leur nom, date de naissance, nationalité, les livres publiés, etc) et des données concernant les livres (le titre, la date de publication, le genre, le ou les auteurs, etc). Un auteur peut avoir écrit plusieurs livres, et un livre peut avoir été écrit par plusieurs auteurs.

Décrivez très brièvement comment vous modéliseriez les données dans une base SQL.